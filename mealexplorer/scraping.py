# -*- coding: utf-8 -*-
#
# Copyright (c) 2018 Thomas Weber <pokehaxco@gmail.com>
#
# SPDX-License-Identifier: MIT
#

#
# This module implements all scraping-related operations.
#

import bs4.element
import io
import logging
import os
import pycurl
import re
import time

from abc import ABC, abstractmethod
from bs4 import BeautifulSoup


class AbstractScraper(ABC):
    """
    This class is the base of every scraper. When an update is requested, the
    meals in the database will be re-fetched from the restaurant website (or
    from a fallback page if it's not available).
    """

    def download_file(self, url):
        """
        Using the Python bindings to curl, downloads a file over the internet.

        Arguments:
        url -- URL of the file to download

        Returns:
        Tuple (HTTP status code, content type, file content as bytes)

        Notes:
        Only GET requests are supported. No cookies can be provided. The method
        disguises itself as Firefox in case curl is blocked by the website.
        Redirects cannot be followed.
        """

        # If internet access is disabled, we can't download
        if self._no_internet:
            raise RuntimeError("internet access is disabled")

        curl = pycurl.Curl()
        curl.reset()

        #
        # Set up curl for GET requests:
        #  - Disable HTTP/2 (this requires external library support in curl)
        #  - Set user agent to Firefox 60
        #  - Reset Accept-Encoding to force uncompressed data
        #  - Set up HTTP headers to resemble Firefox a bit (if browser
        #    detection extends beyond the user agent)
        #
        curl.setopt(pycurl.HTTP_VERSION, pycurl.CURL_HTTP_VERSION_1_1)
        curl.setopt(
            pycurl.USERAGENT,
            ('Mozilla/5.0 (X11; Linux x86_64; rv:60.0) Gecko/20100101 '
             'Firefox/60.0')
        )
        curl.setopt(pycurl.ACCEPT_ENCODING, '')
        curl.setopt(pycurl.HTTPHEADER, (
            'Upgrade-Insecure-Requests: 1',
            'Pragma: no-cache',
            'Cache-Control: no-cache'
        ))

        # Perform the download by writing it into a BytesIO object
        curl.setopt(pycurl.URL, url)

        mem_file = io.BytesIO()
        curl.setopt(pycurl.WRITEFUNCTION, mem_file.write)
        curl.perform()
        content = mem_file.getvalue()

        # Create the tuple
        ret = (
            curl.getinfo(pycurl.HTTP_CODE),
            curl.getinfo(pycurl.CONTENT_TYPE),
            content
        )

        # Clean up
        curl.close()
        return ret

    def download_or_fallback(self, url, *args):
        """
        Either downloads the website, or loads the fallback file.

        Arguments:
        url -- URL to download
        *args -- Path components to the fallback file

        Returns:
        Bytes object containing the page content if the download was successful
        File handle to the fallback file otherwise

        Notes:
        Both types of return values can be passed to BeautifulSoup.
        Assumes that the page we download is actually HTML.
        The fallback files must be stored in mealexplorer/fallback/.
        """

        # Download the website (or use the fallback)
        try:
            self._logger.debug(f"Downloading {url}")

            code, content_type, content = self.download_file(url)
            if code != 200:
                raise RuntimeError(
                    f"Server returned HTTP code {code}")
            if not content_type.startswith("text/html"):
                raise RuntimeError(
                    f"Server returned content type {content_type}")
        except Exception as e:
            self._logger.warn(
                f"Downloading webpage failed ({e}), using local fallback"
            )
            content = open(os.path.join(
                os.path.dirname(__file__), 'fallback', *args
            ))

        return content

    @staticmethod
    def extract_capitalized_words(s):
        """
        Extracts capitalized words from a string. Only works for English and
        German language.

        Arguments:
        s -- String to extract words from

        Returns:
        List of capitalized words in s
        """

        return [
            word for word in s.split(' ')
            if re.match(r'^[A-ZÄÖÜ]\S+$', word)
        ]

    @staticmethod
    def iso3166_to_calling_code(iso3166):
        """
        Translates an ISO-3166 code of a country to the country's calling code
        for international calls.

        Arguments:
        iso3166 -- Two-letter ISO-3166 identifier of the country (example: NL)

        Returns:
        Country calling code for the specified country
        """

        return {
            'CA': 1,
            'US': 1,
            'AG': 1,
            'AI': 1,
            'AS': 1,
            'BB': 1,
            'BM': 1,
            'BS': 1,
            'DM': 1,
            'DO': 1,
            'GD': 1,
            'GU': 1,
            'JM': 1,
            'KN': 1,
            'KY': 1,
            'LC': 1,
            'MP': 1,
            'MS': 1,
            'PR': 1,
            'SX': 1,
            'TC': 1,
            'TT': 1,
            'VC': 1,
            'VG': 1,
            'VI': 1,
            'UM': 1,
            'EG': 20,
            'SS': 211,
            'MA': 212,
            'EH': 212,
            'DZ': 213,
            'TN': 216,
            'LY': 218,
            'GM': 220,
            'SN': 221,
            'MR': 222,
            'ML': 223,
            'GN': 224,
            'CI': 225,
            'BF': 226,
            'NE': 227,
            'TG': 228,
            'BJ': 229,
            'MU': 230,
            'LR': 231,
            'SL': 232,
            'GH': 233,
            'NG': 234,
            'TD': 235,
            'CF': 236,
            'CM': 237,
            'CV': 238,
            'ST': 239,
            'GQ': 240,
            'GA': 241,
            'CG': 242,
            'CD': 243,
            'AO': 244,
            'GW': 245,
            'IO': 246,
            'AC': 247,
            'SC': 248,
            'SD': 249,
            'RW': 250,
            'ET': 251,
            'SO': 252,
            'DJ': 253,
            'KE': 254,
            'TZ': 255,
            'UG': 256,
            'BI': 257,
            'MZ': 258,
            'ZM': 260,
            'MG': 261,
            'RE': 262,
            'YT': 262,
            'TF': 262,
            'ZW': 263,
            'NA': 264,
            'MW': 265,
            'LS': 266,
            'BW': 267,
            'SZ': 268,
            'KM': 269,
            'ZA': 27,
            'SH': 290,
            'TA': 290,
            'ER': 291,
            'AW': 297,
            'FO': 298,
            'GL': 299,
            'GR': 30,
            'NL': 31,
            'BE': 32,
            'FR': 33,
            'ES': 34,
            'GI': 350,
            'PT': 351,
            'LU': 352,
            'IE': 353,
            'IS': 354,
            'AL': 355,
            'MT': 356,
            'CY': 357,
            'FI': 358,
            'AX': 358,
            'BG': 359,
            'HU': 36,
            'LT': 370,
            'LV': 371,
            'EE': 372,
            'MD': 373,
            'AM': 374,
            'QN': 374,
            'BY': 375,
            'AD': 376,
            'MC': 377,
            'SM': 378,
            'VA': 379,
            'UA': 380,
            'RS': 381,
            'ME': 382,
            'XK': 383,
            'HR': 385,
            'SI': 386,
            'BA': 387,
            'EU': 388,
            'MK': 389,
            'IT': 39,
            'RO': 40,
            'CH': 41,
            'CZ': 420,
            'SK': 421,
            'LI': 423,
            'AT': 43,
            'GB': 44,
            'UK': 44,
            'GG': 44,
            'IM': 44,
            'JE': 44,
            'DK': 45,
            'SE': 46,
            'NO': 47,
            'SJ': 47,
            'BV': 47,
            'PL': 48,
            'DE': 49,
            'FK': 500,
            'GS': 500,
            'BZ': 501,
            'GT': 502,
            'SV': 503,
            'HN': 504,
            'NI': 505,
            'CR': 506,
            'PA': 507,
            'PM': 508,
            'HT': 509,
            'PE': 51,
            'MX': 52,
            'CU': 53,
            'AR': 54,
            'BR': 55,
            'CL': 56,
            'CO': 57,
            'VE': 58,
            'GP': 590,
            'BL': 590,
            'MF': 590,
            'BO': 591,
            'GY': 592,
            'EC': 593,
            'GF': 594,
            'PY': 595,
            'MQ': 596,
            'SR': 597,
            'UY': 598,
            'BQ': 599,
            'CW': 599,
            'MY': 60,
            'AU': 61,
            'CX': 61,
            'CC': 61,
            'ID': 62,
            'PH': 63,
            'NZ': 64,
            'PN': 64,
            'SG': 65,
            'TH': 66,
            'TL': 670,
            'NF': 672,
            'AQ': 672,
            'HM': 672,
            'BN': 673,
            'NR': 674,
            'PG': 675,
            'TO': 676,
            'SB': 677,
            'VU': 678,
            'FJ': 679,
            'PW': 680,
            'WF': 681,
            'CK': 682,
            'NU': 683,
            'WS': 685,
            'KI': 686,
            'NC': 687,
            'TV': 688,
            'PF': 689,
            'TK': 690,
            'FM': 691,
            'MH': 692,
            'RU': 7,
            'KZ': 7,
            'JP': 81,
            'KR': 82,
            'VN': 84,
            'KP': 850,
            'HK': 852,
            'MO': 853,
            'KH': 855,
            'LA': 856,
            'CN': 86,
            'BD': 880,
            'TW': 886,
            'TR': 90,
            'CT': 90,
            'IN': 91,
            'PK': 92,
            'AF': 93,
            'LK': 94,
            'MM': 95,
            'MV': 960,
            'LB': 961,
            'JO': 962,
            'SY': 963,
            'IQ': 964,
            'KW': 965,
            'SA': 966,
            'YE': 967,
            'OM': 968,
            'PS': 970,
            'AE': 971,
            'IL': 972,
            'BH': 973,
            'QA': 974,
            'BT': 975,
            'MN': 976,
            'NP': 977,
            'IR': 98,
            'TJ': 992,
            'TM': 993,
            'AZ': 994,
            'GE': 995,
            'KG': 996,
            'UZ': 998
        }[iso3166]

    def __init__(self, database, no_internet):
        """
        Creates a new scraper for a restaurant website.

        Arguments:
        database -- Instance of a MealDatabase where scraped information will
                    be written to.
        no_internet -- True to disable internet access for the scrapers,
                       otherwise False

        Returns:
        Nothing
        """

        self._database = database
        self._no_internet = no_internet

    @abstractmethod
    def update(self, add_restaurant_info):
        """
        Updates the restaurant's meals. Through the Processor class, it will
        not be called more than once within an update interval period.
        If the restaurant has not been scraped once, inserts the restaurant and
        all meals. Otherwise, update the meals of restaurants which require an
        update.

        Arguments:
        add_restaurant_info -- Set to True if the scraper has no restaurants in
                               the database (in which case they should be
                               added), False otherwise.

        Returns:
        Nothing

        Raises:
        Any sort of exception if the operation fails
        """

        return


class UschisPizzaserviceScraper(AbstractScraper):
    """
    Scraper for the delivery service "Uschis Pizzaservice", hosted on Amazon
    servers used by aggregator Lieferando.

    I'm ***NOT*** scraping Lieferando (it's not allowed by the assignment
    description), but the website below this comment. But the operators just
    submit their menu to Lieferando and let them render the website on the
    domain the operators own because they don't (or no longer?) operate their
    own website.
    """

    WEBSITE = 'https://www.uschis-pizzaservice.de/'

    def __init__(self, database, no_internet):
        """
        Creates a new scraper for a restaurant website.

        Arguments:
        database -- Instance of a MealDatabase where scraped information will
                    be written to.
        no_internet -- True to disable internet access for the scrapers,
                       otherwise False

        Returns:
        Nothing
        """

        # Initialize the base class
        super().__init__(database, no_internet)

        # Initialize logger for this class
        self._class_name = type(self).__name__
        self._logger = logging.getLogger(self._class_name)

    def _add_restaurant_info(self, html_soup):
        """
        Adds the information for this restaurant to the database.

        Arguments:
        html_soup -- BeautifulSoup parser tree of the restaurant's website

        Returns:
        ID of the restaurant
        """

        # There's a <div itemtype="Restaurant"> with all the info required
        meta_div = html_soup.find('div',
                                  itemtype='http://schema.org/Restaurant')
        address_div = meta_div.find('div', itemprop='address')

        # Extract all information before inserting into the database
        r_name = meta_div.h2.text
        r_address = [
            address_div.find('span', itemprop='streetAddress').get_text(),
            address_div.find('span', itemprop='postalCode').get_text() + " " +
            address_div.find('span', itemprop='addressLocality').get_text()
        ]
        r_country = (
            "DE"
            if html_soup.find(
                # Lieferando only operates in Germany
                lambda t: t.name == 'script' and 'lieferando.' in t.get_text()
            )
            else "XX"
        )
        r_phone = meta_div.find('span', itemprop='telephone').get_text()
        r_phone = "+%d%s" % (
            AbstractScraper.iso3166_to_calling_code(r_country),
            re.match(r'^0([0-9]+)$', r_phone).group(1)
        )

        self._logger.debug(f"Name: {r_name}")
        self._logger.debug(f"Address: {r_address}")
        self._logger.debug(f"Country: {r_country}")
        self._logger.debug(f"Phone: {r_phone}")
        self._logger.debug(f"Website: {self.WEBSITE}")

        # Insert into the database. The timestamp will be updated anyway, so
        # set it to 0.
        return self._database.insert_restaurant(
            self._class_name, r_name, r_address, r_country, r_phone,
            self.WEBSITE, 0
        )

    def _parse_category(self, category_div, restaurant_id):
        """
        Parses a category from the HTML page and adds the meals from this
        category.

        Arguments:
        category_div -- <div> element of the category with all meals inside
        restaurant_id -- ID of the restaurant to associate meals in the
                         database with this restaurant

        Returns:
        Nothing
        """

        for li in category_div.find_all('li'):
            # Parse data
            m_name = li.find('b', itemprop='name').get_text()
            m_price = float(
                re.sub(r'^([0-9]+),([0-9]{2}) €$',
                       r'\1.\2',
                       li.find('span', itemprop='price').get_text())
            )
            m_ingredients = None

            # Parse ingredients through description first. Extract capitalized
            # words. Avoid empty sets.
            description = li.find('span', itemprop='description')
            if description:
                more_ingredients = self.extract_capitalized_words(
                    description.get_text().replace(',', '')
                )
                if len(more_ingredients) > 0:
                    m_ingredients = set(more_ingredients)

            # Uschis Pizzaservice has the same meal in different sizes, split
            # the size off
            m = re.match(r'^([^\[]+) \[([^\]]+)\]$', m_name)
            if m:
                m_extradata = m.group(2)
                m_name = m.group(1)
            else:
                m_extradata = None

            # Sometimes ingredients are in the title. Also check for pizzas,
            # they don't have 'Mit'. The ingredients must remain in the title.
            # Avoid empty sets.
            m = re.match(r'^(?:Pizza|.+ Mit) (.+)$', m_name)
            if m:
                more_ingredients = self.extract_capitalized_words(
                    m.group(1).replace(',', '').replace(' Und ', ' ')
                )

                # Add to existing set, or create a new one
                if len(more_ingredients) > 0:
                    if m_ingredients:
                        m_ingredients.update(more_ingredients)
                    else:
                        m_ingredients = set(more_ingredients)

            self._logger.debug(
                f"meal: name={m_name}, extra={m_extradata}, price={m_price}, "
                f"ingredients={m_ingredients}"
            )

            # Add it to the database
            new_id = self._database.insert_meal(
                restaurant_id, m_name, m_price, 'EUR', m_ingredients
            )
            self._logger.debug(f"inserted {m_name} (id={new_id})")

    def update(self, add_restaurant_info):
        """
        Updates the restaurant's meals. Through the Processor class, it will
        not be called more than once within an update interval period.
        If the restaurant has not been scraped once, inserts the restaurant and
        all meals. Otherwise, update the meals of restaurants which require an
        update.

        Arguments:
        add_restaurant_info -- Set to True if the scraper has no restaurants in
                               the database (in which case they should be
                               added), False otherwise.

        Returns:
        Nothing

        Raises:
        Any sort of exception if the operation fails
        """

        # Download the website (or use the fallback)
        content = self.download_or_fallback(
            self.WEBSITE, 'uschi.html'
        )

        # Parse the HTML source
        html_soup = BeautifulSoup(content, "html5lib", from_encoding="UTF-8")

        #
        # Add restaurant info if missing. Otherwise, we need our ID for
        # inserting meals.
        #
        if add_restaurant_info:
            restaurant_id = self._add_restaurant_info(html_soup)
        else:
            restaurant_id = None
            for r in self._database.query_restaurants_by_scraper_id(
                    self._class_name):
                if restaurant_id:
                    self._logger.warning(
                        f"multiple restaurants for {restaurant_id} in database"
                    )
                else:
                    restaurant_id = r['id']

        # To parse meals, we need the highest category ID so we know how many
        # category <divs> we can iterate
        category_list = html_soup.find(
            lambda t: t.name == 'div' and
            'categorylist' in t.get('class', '')
        ).ul
        last_category = max([
            int(re.match(r'^#cat([0-9]+)$', t.get('href', '')).group(1), 10)
            for t in
            category_list.find_all(
                lambda t: t.name == 'a' and
                re.match(r'^#cat([0-9]+)$', t.get('href', ''))
            )
        ])

        # Parse categories and add meals from each category
        for i in range(1, last_category + 1):
            category_div = html_soup.find('div', id='cat%d' % i)
            self._parse_category(category_div, restaurant_id)

        # Update the restaurant's last-update field
        self._database.update_restaurant_last_update(restaurant_id,
                                                     int(time.time()))

        # Commit all changes into the database
        self._database.commit()


class StehpizzeriaNapoliScraper(AbstractScraper):
    """
    Scraper for the pizzeria "Stehpizzeria Napoli" (which is NOT a delivery
    service).
    """

    WEBSITE = 'http://www.stehpizzeria-napoli.de/index.php'

    def __init__(self, database, no_internet):
        """
        Creates a new scraper for a restaurant website.

        Arguments:
        database -- Instance of a MealDatabase where scraped information will
                    be written to.
        no_internet -- True to disable internet access for the scrapers,
                       otherwise False

        Returns:
        Nothing
        """

        # Initialize the base class
        super().__init__(database, no_internet)

        # Initialize logger for this class
        self._class_name = type(self).__name__
        self._logger = logging.getLogger(type(self).__name__)

    def _add_restaurant_info(self):
        """
        Adds the information for this restaurant to the database.

        Arguments:
        None

        Returns:
        ID of the restaurant
        """

        # Download the imprint page (or use the fallback)
        content = self.download_or_fallback(
            self.WEBSITE + '/impressum.html',
            'napoli', 'impressum.html'
        )

        # Parse the HTML source
        html_soup = BeautifulSoup(content, "html5lib", from_encoding="UTF-8")

        address_par = html_soup.find(
            'span', class_='green', text='Anschrift:'
        ).parent

        # Extract info from page
        r_name = html_soup.head.title.get_text().split(' - ')[0]
        r_address = [x.next_sibling for x in address_par.find_all('br')]

        # Detect <html lang="de-de">
        r_country = (
            "DE" if html_soup.html.get('lang', '').lower() == 'de-de'
            else "XX"
        )

        phone_par = html_soup.find(
            lambda t: t.name == 'p' and t.text.startswith('Telefon:')
        )
        r_phone = re.sub(r'[^0-9]', r'',
                         phone_par.contents[0].replace('Telefon:', ''))
        r_phone = "+%d%s" % (
            AbstractScraper.iso3166_to_calling_code(r_country),
            re.match(r'^0([0-9]+)$', r_phone).group(1)
        )

        self._logger.debug(f"Name: {r_name}")
        self._logger.debug(f"Address: {r_address}")
        self._logger.debug(f"Country: {r_country}")
        self._logger.debug(f"Phone: {r_phone}")
        self._logger.debug(f"Website: {self.WEBSITE}")

        # Insert into the database. The timestamp will be updated anyway, so
        # set it to 0.
        return self._database.insert_restaurant(
            self._class_name, r_name, r_address, r_country, r_phone,
            self.WEBSITE, 0
        )

    def _parse_menu_page(self, basename, restaurant_id):
        """
        Parses the page of a menu. The website split its menu into multiple
        pages.

        Arguments:
        basename -- Basename to load (self.WEBSITE + '/' + basename + '.html')
        restaurant_id -- ID of the restaurant in the database

        Returns:
        Nothing
        """

        # Pizza table needs more handling, as well as drinks
        pizza = basename == 'home'
        drinks = basename == 'getraenke'

        # Download the imprint page (or use the fallback)
        content = self.download_or_fallback(
            self.WEBSITE + '/' + basename + '.html',
            'napoli', basename + '.html'
        )

        # Parse the HTML source
        html_soup = BeautifulSoup(content, "html5lib", from_encoding="UTF-8")

        # Find the meal table
        meal_table = html_soup.find(
            'caption', text=re.compile('!!! KEIN LIEFERSERVICE !!!')
        ).parent

        for i, row in enumerate(meal_table.find_all('tr')):
            tds = row.find_all('td')

            # Select price for large pizza (column 4). I love it expensive.
            price_col = tds[3 if pizza else 2].get_text().strip()
            if not price_col.endswith('€'):
                # Row does not include a meal
                continue

            # Second column contains name and ingredients
            name_ingredients_col = tds[1]

            # Extract name
            m_name = name_ingredients_col.contents[0]
            if type(m_name) is bs4.element.Tag:
                m_name = m_name.get_text()
            m_name = re.sub(r',$', r'', m_name.strip())

            # Also get rid of quote characters to align with Uschi
            m_name = re.sub(r'"', r'', m_name)

            # Prefix with pizza if required
            if (pizza and not m_name.startswith("Pizza") and
                    not ('Belag' in m_name or 'zzgl.' in m_name)):
                m_name = 'Pizza ' + m_name

            # If it's a drink and actually contains multiple entries, split
            # them and add them all with the same price
            if drinks and ('/' in m_name or 'etc.' in m_name):
                m_name = self.extract_capitalized_words(
                    re.sub(r'[,/]', r' ', m_name)
                )

            # Extract ingredients unless it's drinks
            if not drinks and name_ingredients_col.find('br'):
                # Work around markup fuckup (<br> inside <span>)
                br = name_ingredients_col.find('br')
                if br.parent.name == 'span':
                    m_ingredients = br.parent.next_sibling
                else:
                    m_ingredients = br.next_sibling

                m_ingredients = self.extract_capitalized_words(
                    re.sub(r'[,()]', r'', m_ingredients)
                )
                m_ingredients = (
                    set(m_ingredients) if len(m_ingredients) > 0 else None
                )
            else:
                m_ingredients = None

            # Finally the price. It's also inconsistent.
            m_price = float(
                re.sub(r'^(?:ab\s+)?([0-9]+)[,\.]([0-9]{2})\s+€$',
                       r'\1.\2',
                       price_col.strip())
            )

            self._logger.debug(
                f"meal: name={m_name}, price={m_price}, "
                f"ingredients={m_ingredients}"
            )

            # Add the meal(s) to the database
            if isinstance(m_name, str):
                m_name = [m_name]
            for meal in m_name:
                new_id = self._database.insert_meal(
                    restaurant_id, meal, m_price, 'EUR', m_ingredients
                )
                self._logger.debug(f"inserted {meal} (id={new_id})")

    def update(self, add_restaurant_info):
        """
        Updates the restaurant's meals. Through the Processor class, it will
        not be called more than once within an update interval period.
        If the restaurant has not been scraped once, inserts the restaurant and
        all meals. Otherwise, update the meals of restaurants which require an
        update.

        Arguments:
        add_restaurant_info -- Set to True if the scraper has no restaurants in
                               the database (in which case they should be
                               added), False otherwise.

        Returns:
        Nothing

        Raises:
        Any sort of exception if the operation fails
        """

        #
        # Add restaurant info if missing. Otherwise, we need our ID for
        # inserting meals.
        #
        if add_restaurant_info:
            restaurant_id = self._add_restaurant_info()
        else:
            restaurant_id = None
            for r in self._database.query_restaurants_by_scraper_id(
                    self._class_name):
                if restaurant_id:
                    self._logger.warning(
                        f"multiple restaurants for {restaurant_id} in database"
                    )
                else:
                    restaurant_id = r['id']

        #
        # Add the meals. They're stored in multiple files on the website.
        #
        meal_pages = (
            'home', 'pizzabroetchen-pizzaroellchen', 'nudeln', 'salate',
            'gemuese', 'getraenke'
        )
        for page in meal_pages:
            self._parse_menu_page(page, restaurant_id)

        # Update the restaurant's last-update field
        self._database.update_restaurant_last_update(restaurant_id,
                                                     int(time.time()))

        # Commit all changes into the database
        self._database.commit()


class AmiRestaurantScraper(AbstractScraper):
    """
    Scraper for the French restaurant "Ami Restaurant".
    """

    WEBSITE = 'http://www.ami-restaurant.de/'

    def __init__(self, database, no_internet):
        """
        Creates a new scraper for a restaurant website.

        Arguments:
        database -- Instance of a MealDatabase where scraped information will
                    be written to.
        no_internet -- True to disable internet access for the scrapers,
                       otherwise False

        Returns:
        Nothing
        """

        # Initialize the base class
        super().__init__(database, no_internet)

        # Initialize logger for this class
        self._class_name = type(self).__name__
        self._logger = logging.getLogger(type(self).__name__)

    def _add_restaurant_info(self):
        """
        Adds the information for this restaurant to the database.

        Arguments:
        None

        Returns:
        ID of the restaurant
        """

        # Download the imprint page (or use the fallback)
        content = self.download_or_fallback(
            self.WEBSITE + 'impressum/',
            'ami', 'impressum.html'
        )

        # Parse the HTML source
        html_soup = BeautifulSoup(content, "html5lib", from_encoding="UTF-8")

        # Look for a <div> 'content', go to the first <p> with all data inside
        imprint_p = html_soup.find('div', id='content')
        imprint_p = imprint_p.find('p')

        # Detect <html lang="de-de">
        r_country = (
            "DE" if html_soup.html.get('lang', '').lower() == 'de-de'
            else "XX"
        )

        # Extract contact information from lines
        for i, line in enumerate(imprint_p.stripped_strings):
            if i == 0:
                r_name = line
            elif i == 2:
                r_address = [line]
            elif i == 3:
                r_address.append(line)
            elif i == 4:
                r_phone = re.sub(r'\s', r'', re.sub(r'^Tel\.: ', r'', line))

        self._logger.debug(f"Name: {r_name}")
        self._logger.debug(f"Address: {r_address}")
        self._logger.debug(f"Country: {r_country}")
        self._logger.debug(f"Phone: {r_phone}")
        self._logger.debug(f"Website: {self.WEBSITE}")

        # Insert into the database. The timestamp will be updated anyway, so
        # set it to 0.
        return self._database.insert_restaurant(
            self._class_name, r_name, r_address, r_country, r_phone,
            self.WEBSITE, 0
        )

    def _parse_menu_div(self, element, restaurant_id):
        """
        Parses the <div> element of the meal.

        Arguments:
        element -- <div> element of the meal
        restaurant_id -- ID of the restaurant in the database

        Returns:
        Nothing
        """

        # Must have <h5>, which is the meal's name
        caption_h5 = element.find('h5')
        if not caption_h5:
            return
        m_name = caption_h5.get_text()

        # Next to it is a <p>. Two lines if there are ingredients, the last
        # line is always the price.
        rest_p = caption_h5.find_next_sibling('p')
        rest_p_lines = list(rest_p.stripped_strings)
        assert len(rest_p_lines) in (1, 2)

        m_price = float(
            re.sub(r'^([0-9]+),([0-9]{2})\s*€$', r'\1.\2', rest_p_lines[-1])
        )
        if len(rest_p_lines) == 2:
            m_ingredients = self.extract_capitalized_words(
                re.sub(r',', r' ', rest_p_lines[0])
            )
            m_ingredients = (
                set(m_ingredients) if len(m_ingredients) > 0 else None
            )
        else:
            m_ingredients = None

        self._logger.debug(
            f"meal: name={m_name}, price={m_price}, "
            f"ingredients={m_ingredients}"
        )

        # Insert meal into the database
        new_id = self._database.insert_meal(
            restaurant_id, m_name, m_price, 'EUR', m_ingredients
        )
        self._logger.debug(f"inserted {m_name} (id={new_id})")

    def update(self, add_restaurant_info):
        """
        Updates the restaurant's meals. Through the Processor class, it will
        not be called more than once within an update interval period.
        If the restaurant has not been scraped once, inserts the restaurant and
        all meals. Otherwise, update the meals of restaurants which require an
        update.

        Arguments:
        add_restaurant_info -- Set to True if the scraper has no restaurants in
                               the database (in which case they should be
                               added), False otherwise.

        Returns:
        Nothing

        Raises:
        Any sort of exception if the operation fails
        """

        #
        # Add restaurant info if missing. Otherwise, we need our ID for
        # inserting meals.
        #
        if add_restaurant_info:
            restaurant_id = self._add_restaurant_info()
        else:
            restaurant_id = None
            for r in self._database.query_restaurants_by_scraper_id(
                    self._class_name):
                if restaurant_id:
                    self._logger.warning(
                        f"multiple restaurants for {restaurant_id} in database"
                    )
                else:
                    restaurant_id = r['id']

        # Download the meals page (or use the fallback)
        content = self.download_or_fallback(
            self.WEBSITE + 'speisekarte/',
            'ami', 'speisekarte.html'
        )

        # Parse the HTML source
        html_soup = BeautifulSoup(content, "html5lib", from_encoding="UTF-8")

        # Find all Bootstrap-like containers with a <h5> indicating meal name
        for element in html_soup.find_all(
                'div', class_='col medium-4 small-12 large-4'):
            self._parse_menu_div(element, restaurant_id)

        # Update the restaurant's last-update field
        self._database.update_restaurant_last_update(restaurant_id,
                                                     int(time.time()))

        # Commit all changes into the database
        self._database.commit()


class LaProvenceScraper(AbstractScraper):
    """
    Scraper for the French restaurant "La Provence".
    """

    WEBSITE = 'http://la-provence-leipzig.de/'

    def __init__(self, database, no_internet):
        """
        Creates a new scraper for a restaurant website.

        Arguments:
        database -- Instance of a MealDatabase where scraped information will
                    be written to.
        no_internet -- True to disable internet access for the scrapers,
                       otherwise False

        Returns:
        Nothing
        """

        # Initialize the base class
        super().__init__(database, no_internet)

        # Initialize logger for this class
        self._class_name = type(self).__name__
        self._logger = logging.getLogger(type(self).__name__)

    def _add_restaurant_info(self, html_soup):
        """
        Adds the information for this restaurant to the database.

        Arguments:
        html_soup -- BeautifulSoup parser tree of the restaurant's website

        Returns:
        ID of the restaurant
        """

        # Detect <html lang="de-de">
        r_country = (
            "DE" if html_soup.html.get('lang', '').lower() == 'de-de'
            else "XX"
        )

        # All information is in the title
        title = html_soup.head.title.get_text().split('–')[0]
        for i, line in enumerate([t.strip() for t in title.split('·')]):
            if i == 0:
                r_name = line
            elif i == 1:
                r_address = [line]
            elif i == 2:
                r_address.append(line)
            elif i == 3:
                r_phone = re.sub(r'Telefon:\s*[0-9]([0-9\s]+)', r'\1', line)
                r_phone = "+%d%s" % (
                    AbstractScraper.iso3166_to_calling_code(r_country),
                    re.sub(r'\s', r'', r_phone)
                )

        self._logger.debug(f"Name: {r_name}")
        self._logger.debug(f"Address: {r_address}")
        self._logger.debug(f"Country: {r_country}")
        self._logger.debug(f"Phone: {r_phone}")
        self._logger.debug(f"Website: {self.WEBSITE}")

        # Insert into the database. The timestamp will be updated anyway, so
        # set it to 0.
        return self._database.insert_restaurant(
            self._class_name, r_name, r_address, r_country, r_phone,
            self.WEBSITE, 0
        )

    def _parse_menu_div(self, element, restaurant_id):
        """
        Parses the <div> element of the meal.

        Arguments:
        element -- <div> element of the meal
        restaurant_id -- ID of the restaurant in the database

        Returns:
        Nothing
        """

        # Get the <div> elements whose ID starts with 'content-heading'.
        # From there, each <p> which centers the text contains one meal.
        for div in element.find_all('div', id=re.compile('^content-heading')):
            for meal_p in div.find_all('p', style=re.compile('text-align')):

                # <p>'s having an image are junk decoration
                if meal_p.find('img'):
                    continue

                # Extract just the text (some meals don't have a <strong> tag,
                # and we still have empty ones). We need at least meal and
                # price, they're on a different line.
                meal = list(meal_p.stripped_strings)
                if len(meal) < 2:
                    continue

                # <strong> without text is not good:
                # "Unsere spritzigen und fruchtigen Sommegetränke"
                strong = meal_p.strong
                if strong:
                    if strong.next_sibling is None:
                        continue

                # First line is always the name
                m_name = meal.pop(0)

                # Split list into two, ingredients and prices.
                # https://stackoverflow.com/a/12135169
                m_ingredients = []
                m_prices = []
                for line in meal:
                    (m_ingredients, m_prices)['€' in line].append(line)

                # Extract capitalized words in ingredients
                if len(m_ingredients) > 0:
                    m_ingredients = self.extract_capitalized_words(
                        re.sub(r',', r' ',
                               ' '.join(m_ingredients))
                    )
                    m_ingredients = (
                        set(m_ingredients) if len(m_ingredients) > 0 else None
                    )
                else:
                    m_ingredients = None

                # Parse price (and variants)
                assert len(m_prices) > 0
                for line in m_prices:
                    m = re.match(
                        r'^(.*\s\s+|[^:]+:\s*)?([0-9]+),([0-9]{2})\s*€$',
                        line
                    )
                    if not m:
                        raise RuntimeError("%s doesn't match regex" % line)

                    # Small fixup for extra data
                    m_extradata = re.sub(
                        r':$', r'', (m.group(1) or '').strip()
                    )
                    if m_extradata == '':
                        m_extradata = None

                    # Make price a number
                    m_price = float(
                        "%s.%s" % (m.group(2), m.group(3))
                    )

                    self._logger.debug(
                        f"meal: name={m_name}, extra={m_extradata}, "
                        f"price={m_price}, ingredients={m_ingredients}"
                    )

                    # Insert meal into the database
                    new_id = self._database.insert_meal(
                        restaurant_id, m_name, m_price, 'EUR', m_ingredients
                    )
                    self._logger.debug(f"inserted {m_name} (id={new_id})")

    def update(self, add_restaurant_info):
        """
        Updates the restaurant's meals. Through the Processor class, it will
        not be called more than once within an update interval period.
        If the restaurant has not been scraped once, inserts the restaurant and
        all meals. Otherwise, update the meals of restaurants which require an
        update.

        Arguments:
        add_restaurant_info -- Set to True if the scraper has no restaurants in
                               the database (in which case they should be
                               added), False otherwise.

        Returns:
        Nothing

        Raises:
        Any sort of exception if the operation fails
        """

        # Download the website (or use the fallback)
        content = self.download_or_fallback(
            self.WEBSITE, 'provence.html'
        )

        # Parse the HTML source
        html_soup = BeautifulSoup(content, "html5lib", from_encoding="UTF-8")

        #
        # Add restaurant info if missing. Otherwise, we need our ID for
        # inserting meals.
        #
        if add_restaurant_info:
            restaurant_id = self._add_restaurant_info(html_soup)
        else:
            restaurant_id = None
            for r in self._database.query_restaurants_by_scraper_id(
                    self._class_name):
                if restaurant_id:
                    self._logger.warning(
                        f"multiple restaurants for {restaurant_id} in database"
                    )
                else:
                    restaurant_id = r['id']

        #
        # We only care about certain <div> elements:
        #  - abendkarte
        #  - weine
        #  - getraenkekarte
        #
        for element in ('abendkarte', 'weine', 'getraenkekarte'):
            self._parse_menu_div(html_soup.find('div', id=element),
                                 restaurant_id)

        # Update the restaurant's last-update field
        self._database.update_restaurant_last_update(restaurant_id,
                                                     int(time.time()))

        # Commit all changes into the database
        self._database.commit()
