# -*- coding: utf-8 -*-
#
# Copyright (c) 2018 Thomas Weber <pokehaxco@gmail.com>
#
# SPDX-License-Identifier: MIT
#

#
# This module implements the frontend of Meal Explorer, which also hooks up the
# backend's REST API under one HTTP server.
#

import email.utils
import http.server
import logging
import os
import re

from http import HTTPStatus
from urllib.parse import urlparse


class FrontendRequestHandler(http.server.BaseHTTPRequestHandler):
    """
    Subclass of BaseHTTPRequestHandler to serve static frontend files together
    with the application's REST API.
    """

    def __init__(self, htdocs_path, rest_api, *args):
        """
        Initializes a new request handler.

        Arguments:
        htdocs_path -- Full path to the directory with static files
        rest_api -- Instance of the REST API
        *args -- Arguments for the BaseHTTPRequestHandler constructor

        Returns:
        Nothing
        """

        # Initialize scraper for this class
        self._logger = logging.getLogger(type(self).__name__)

        self._htdocs_path = htdocs_path
        self._rest_api = rest_api

        # Initialize the superclass
        super().__init__(*args)

        # Switch to HTTP 1.1
        self.protocol_version = 'HTTP/1.1'

    def _send_error_page(self, code):
        """
        Sends an error page to the client.

        Arguments:
        code -- HTTP status code to send
        """

        # Encode content as UTF-8
        content = (
            ("<html><head><title>%d %s</title></head><body><h1>%d %s</h1>"
             "</body></html>") % (code, code.phrase, code, code.phrase)
        ).encode('UTF-8')

        self.send_response(code)
        self.send_header("Content-Type", "text/html; charset=UTF-8")
        self.send_header("Content-Length", str(len(content)))
        self.end_headers()
        self.wfile.write(content)

    def _guess_mime_type(self, fp, filename):
        """
        Guesses the MIME type of the file to transmit, first by peeking into
        the contents, then based on the filename.

        Arguments:
        fp -- File handle
        filename -- Basename of the file opened as fp

        Returns:
        String describing the MIME type of the content
        """

        # Data first
        data = fp.read(128)
        fp.seek(0, os.SEEK_SET)

        if data[:14] == r'<!DOCTYPE html':
            return 'text/html; charset=UTF-8'
        elif data[:8] == bytes([0x89, 0x50, 0x4E, 0x47, 0xD, 0xA, 0x1A, 0xA]):
            return 'image/png'
        elif data[:6] in (r'GIF87a', r'GIF89a'):
            return 'image/gif'
        elif data[:3] == bytes([0xFF, 0xD8, 0xFF]):
            return 'image/jpeg'

        # Now based on the filename
        filename_root, filename_ext = os.path.splitext(filename)
        try:
            return {
                '.html': 'text/html; charset=UTF-8',
                '.htm': 'text/html; charset=UTF-8',
                '.txt': 'text/plain',
                '.log': 'text/plain',
                '.gif': 'image/gif',
                '.jpg': 'image/jpeg',
                '.jpeg': 'image/jpeg',
                '.png': 'image/png',
                '.css': 'text/css',
                '.js': 'application/javascript'
            }[filename_ext]
        except KeyError:
            return "application/octet-stream"

    def _send_file(self, htdocs_path, if_modified_since=None):
        """
        Transmits a local file as HTTP response. The MIME type of the file is
        guessed based on magic values or the file extension.

        Arguments:
        htdocs_path -- Path to the local file to transmit
        if_modified_since -- Value of the If-Modified-Since header field,
                             if present

        Returns:
        Nothing
        """

        with open(htdocs_path, 'rb') as fp:
            # Query file information
            st = os.fstat(fp.fileno())

            # Check last modification date. If local file is not newer than
            # the browser has, 304 is enough.
            send_304 = False
            if if_modified_since:
                ims_time = int(email.utils.mktime_tz(
                    email.utils.parsedate_tz(if_modified_since)))
                file_time = int(st.st_mtime)
                send_304 = ims_time >= file_time

            # Send status code
            self.send_response(
                HTTPStatus.NOT_MODIFIED if send_304 else HTTPStatus.OK
            )

            # Detect MIME type and send it
            self.send_header(
                'Content-Type',
                self._guess_mime_type(fp, os.path.basename(htdocs_path))
            )

            # Send content length and last modification time (for caching).
            # st_mtime is already UTC (on Unix, at least)
            self.send_header(
                'Content-Length',
                '0' if send_304 else str(st.st_size)
            )
            self.send_header(
                'Last-Modified',
                email.utils.formatdate(st.st_mtime, usegmt=True)
            )

            # Send the file
            self.end_headers()
            if not send_304:
                self.wfile.write(fp.read())

    def _handle_api_requests(self, url, method_name):
        """
        Handles requests to API URLs regardless of the HTTP method.

        Arguments:
        url -- urllib ParseResult containing the requested endpoint
        method_name -- Name of method in the RestAPI class to call

        Returns:
        True if the request was handled (it was an API call)
        False otherwise
        """

        if url.path.startswith("/api/"):
            # Remove prefix from path
            url = urlparse(re.sub(r'^/api/', r'', self.path))

            # Pass to implementation
            status_code, content = getattr(self._rest_api, method_name)(url)
            content = content.encode('UTF-8')

            # Build the response
            self.send_response(status_code)
            self.send_header('Content-Type', 'application/json')
            self.send_header('Content-Length', str(len(content)))
            self.send_header('Cache-Control', 'no-cache')
            self.end_headers()
            self.wfile.write(content)
            return True

        return False

    def do_GET(self):
        """
        Handles a HTTP GET request.

        Arguments:
        None

        Returns:
        Nothing
        """

        # Parse the path to ignore query strings
        parsed_path = urlparse(self.path)

        # Handle API endpoints under the /api path
        if self._handle_api_requests(parsed_path, 'do_GET'):
            return

        #
        # Otherwise we serve static files. Look in htdocs
        #

        local_path = os.path.join(self._htdocs_path, parsed_path.path[1:])

        # Check if the index file is requested
        if local_path[-1] == '/':
            for index_file in ('index.html', 'index.htm'):
                local_path_index = local_path + index_file
                if os.path.exists(local_path_index):
                    self._send_file(local_path_index,
                                    self.headers.get('If-Modified-Since'))
                    return
            else:
                # Index file not found and we don't allow directory listings
                self._send_error_page(
                    HTTPStatus.FORBIDDEN if os.path.exists(local_path)
                    else HTTPStatus.NOT_FOUND
                )
                return

        # Send file if it exists
        if os.path.exists(local_path):
            self._send_file(local_path,
                            self.headers.get('If-Modified-Since'))
            return

        # Otherwise, not found
        self._send_error_page(HTTPStatus.NOT_FOUND)

    def do_POST(self):
        """
        Handles a HTTP POST request. POST requests are only used by the API.

        Arguments:
        None

        Returns:
        Nothing
        """

        # Parse the path to ignore query strings
        url = urlparse(self.path)

        # Handle API endpoints under the /api path. Otherwise, raise a 400
        if not self._handle_api_requests(url, 'do_POST'):
            self._send_error_page(HTTPStatus.INVALID_REQUEST)


class FrontendServer(object):
    """
    Operates a HTTP server at the specified address and port, serving static
    files and the REST API.
    """

    def __init__(self, address, port, htdocs_path, rest_api):
        """
        Initializes a new server.

        Arguments:
        server -- Address the server will listen on
        port -- Port the server will listen on
        htdocs_path -- Full path to the directory with static files
        rest_api -- Instance of the REST API

        Returns:
        Nothing
        """

        self._htdocs_path = htdocs_path
        self._rest_api = rest_api

        # Create a handler to supply custom arguments for the request handler
        def __handler(*args):
            FrontendRequestHandler(self._htdocs_path, self._rest_api, *args)

        # Initialize scraper for this class
        self._logger = logging.getLogger(type(self).__name__)

        # Create the server
        self._server_params = (address, port)
        self._server = http.server.HTTPServer(self._server_params, __handler)

    def run(self):
        """
        Operates the HTTP server. It can only be cancelled by sending a
        KeyboardInterrupt to the application.

        Arguments:
        None

        Returns:
        Nothing
        """

        self._logger.info(("Starting server at %s:%d. Press Ctrl-C to stop "
                           "the server.") % self._server_params)

        try:
            self._server.serve_forever()
        except KeyboardInterrupt:
            pass
