# -*- coding: utf-8 -*-
#
# Copyright (c) 2018 Thomas Weber <pokehaxco@gmail.com>
#
# SPDX-License-Identifier: MIT
#

#
# Main entry point when the module is run as program.
#

import sys

from .program import MealExplorer


# Run the program
sys.exit(MealExplorer().run(sys.argv))
