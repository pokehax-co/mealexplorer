# -*- coding: utf-8 -*-
#
# Copyright (c) 2018 Thomas Weber <pokehaxco@gmail.com>
#
# SPDX-License-Identifier: MIT
#

#
# This module implements the REST API, which is plugged into the frontend's
# HTTP server.
#

import json
import logging
import re

from http import HTTPStatus
from urllib.parse import parse_qs


class RestAPI(object):
    """
    Class which implements the REST API commands. It interacts with the
    database and delivers the results over the frontend's HTTP server.
    """

    @staticmethod
    def _make_failure_object(status_code, reason):
        """
        Creates a JSON response indicating failure.

        Arguments:
        status_code -- HTTP status code to return
        reason -- Reason why the API call failed

        Returns:
        Tuple (HTTPStatus, JSON response string)

        Notes:
        Encodes the parameters into the JSON object
        { 'success': False, 'reason': 'why did it fail?' }
        """

        return (status_code, json.dumps({
            'success': False,
            'reason': reason
        }))

    @staticmethod
    def _make_success_object(data):
        """
        Creates a JSON response indicating success.

        Arguments:
        data -- Object to return in the response

        Returns:
        Tuple (HTTPStatus.OK, JSON response string)

        Notes:
        Encodes the parameters into the JSON object
        { 'success': True, 'data': {...} }
        """

        return (HTTPStatus.OK, json.dumps({
            'success': True,
            'data': data
        }))

    @staticmethod
    def _convert_restaurant(row, only_names=False):
        """
        Converts a database row representing a restaurant into an object
        suitable for JSON.

        Arguments:
        row -- Database row to convert. Its values must be accessible over
               named keys ('id', etc.)
        only_names -- True to return only names, False otherwise (optional)

        Returns:
        Python object representing the restaurant

        Note:
        The scraper ID is filtered from the conversion, as it is an
        implementation detail.
        """

        o = {
            'id': row['id'],
            'name': row['name']
        }
        if not only_names:
            o['address'] = row['address'].split('|')
            o['country'] = row['country']
            o['phone'] = row['phone']
            o['website'] = row['website']
            o['last_update_timestamp'] = row['last_update']

        return o

    @staticmethod
    def _convert_meal(row):
        """
        Converts a database row representing a meal into an object suitable for
        JSON.

        Arguments:
        row -- Database row to convert. Its values must be accessible over
               named keys ('id', etc.)

        Returns:
        Python object representing the meal

        Note:
        The scraper ID is filtered from the conversion, as it is an
        implementation detail.
        """

        return {
            'id': row['id'],
            'restaurant_id': row['restaurant_id'],
            'name': row['name'],
            'price': row['price'],
            'currency': row['currency'],
            'ingredients': (
                row['ingredients'].split('|')
                if row['ingredients'] is not None
                else []
            )
        }

    def __init__(self, processor, database):
        """
        Initializes a new REST API handler.

        Arguments:
        processor -- Instance of the update processor
        database -- Instance of the database

        Returns:
        Nothing
        """

        self._database = database
        self._processor = processor

        # Initialize scraper for this class
        self._logger = logging.getLogger(type(self).__name__)

    def _handle_restaurants(self, method, url):
        """
        Handler for querying information on restaurants.

        Arguments:
        method -- HTTP method used when accessing the endpoint
        url -- urllib ParseResult containing the requested endpoint

        Returns:
        Tuple (HTTPStatus, JSON response string)

        Notes:
        This method supports the following URLs:
         - restaurants (all restaurants in database)
         - restaurants?only_names=x (all restaurants in database, names only)
         - restaurants/<id> (restaurant with ID <id>)
        """

        # Only GET requests, sorry
        if method != 'GET':
            return self._make_failure_object(
                HTTPStatus.BAD_REQUEST,
                f"{method} method is not supported"
            )

        # Try to parse an ID. If so, limit the result to the restaurant
        match = re.match(r'restaurants/([0-9]+)', url.path)
        if match:
            restaurant_id = int(match.group(1))
            self._logger.debug(
                f"Querying restaurant with ID {restaurant_id}"
            )

            restaurant = None
            try:
                for r in self._database.query_restaurants_by_id(restaurant_id):
                    # If this fails, then the backend is broken
                    assert restaurant is None
                    restaurant = r
            except AssertionError:
                # don't catch assertion errors, they *have* to kill your app
                raise
            except Exception as e:
                self._logger.exception(("Database query failed because of "
                                        "exception"))
                return self._make_failure_object(
                    HTTPStatus.INTERNAL_SERVER_ERROR,
                    f"Query failed: {e}"
                )

            # Convert to JSON object, or fail if there is no such restaurant
            if restaurant:
                return self._make_success_object(
                    self._convert_restaurant(restaurant)
                )
            else:
                return self._make_failure_object(
                    HTTPStatus.NOT_FOUND,
                    f"Restaurant {restaurant_id} not found"
                )
        else:
            # If there's a query string, check if only restaurant names are
            # requested.
            only_names = False
            if len(url.query) > 0:
                query = parse_qs(url.query)

                # Only use last 'only_names' query, parse as integer
                try:
                    only_names = int(query.get('only_names', [''])[-1]) > 0
                except Exception:
                    only_names = False

            self._logger.debug(
                f"Querying all restaurants (names only: {only_names})"
            )

            try:
                restaurants = list(
                    self._database.query_restaurants_by_id(None)
                )
            except Exception as e:
                self._logger.exception(("Database query failed because of "
                                        "exception"))
                return self._make_failure_object(
                    HTTPStatus.INTERNAL_SERVER_ERROR,
                    f"Query failed: {e}"
                )

            return self._make_success_object([
                self._convert_restaurant(r, only_names) for r in restaurants
            ])

    def _handle_meals(self, method, url):
        """
        Handler for querying information on meals.

        Arguments:
        method -- HTTP method used when accessing the endpoint
        url -- urllib ParseResult containing the requested endpoint

        Returns:
        Tuple (HTTPStatus, JSON response string)

        Notes:
        This method supports the following URLs:
         - meals (all meals in database)
         - meals?q=<keywords>&search=<meals|ingredients> (search for meals)
         - meals/<id> (meal with ID <id>)
        """

        # Only GET requests, sorry
        if method != 'GET':
            return self._make_failure_object(
                HTTPStatus.BAD_REQUEST,
                f"{method} method is not supported"
            )

        # Try to parse an ID. If so, limit the result to the meal
        match = re.match(r'meals/([0-9]+)', url.path)
        if match:
            meal_id = int(match.group(1))
            self._logger.debug(
                f"Querying meal with ID {meal_id}"
            )

            meal = None
            try:
                for r in self._database.query_meals_by_id(meal_id):
                    # If this fails, then the backend is broken
                    assert meal is None
                    meal = r
            except AssertionError:
                # don't catch assertion errors, they *have* to kill your app
                raise
            except Exception as e:
                self._logger.exception(("Database query failed because of "
                                        "exception"))
                return self._make_failure_object(
                    HTTPStatus.INTERNAL_SERVER_ERROR,
                    f"Update failed: {e}"
                )

            # Convert to JSON object, or fail if there is no such meal
            if meal:
                return self._make_success_object(
                    self._convert_meal(meal)
                )
            else:
                return self._make_failure_object(
                    HTTPStatus.NOT_FOUND,
                    f"meal {meal_id} not found"
                )
        else:
            search_query = set()
            search_ingredients = False

            # If there's a query string, then we should perform a search
            # operation.
            if len(url.query) > 0:
                query = parse_qs(url.query)

                # Search for the individual words across all 'q' parameters.
                # Keywords like 'AND' or 'OR' or 'NOT' are not supported (we're
                # not a professional search engine like DDG).
                # parse_qs() decodes percent-encoded characters for us already.
                search_query = set([
                    keyword
                    for q_value in query.get('q', '')
                    for keyword in q_value.split(' ')]
                )

                # Only use last 'in' query
                search_type = query.get('in', [''])[-1].lower()
                if search_type == 'ingredients':
                    search_ingredients = True
                elif search_type in ('', 'meals'):
                    search_ingredients = False
                else:
                    return self._make_failure_object(
                        HTTPStatus.BAD_REQUEST,
                        f"invalid search type {search_type}"
                    )

            try:
                if len(search_query) > 0:
                    self._logger.debug(
                        f"Searching for {search_query} in "
                        f"{'ingredients' if search_ingredients else 'meals'}"
                    )
                    meals = self._database.query_meals_by_search_terms(
                        search_query,
                        'ingredients' if search_ingredients else 'name'
                    )
                else:
                    self._logger.debug(
                        f"Querying all meals"
                    )
                    meals = self._database.query_meals_by_id(None)
            except Exception as e:
                self._logger.exception(("Database query failed because of "
                                        "exception"))
                return self._make_failure_object(
                    HTTPStatus.INTERNAL_SERVER_ERROR,
                    f"Query failed: {e}"
                )

            return self._make_success_object([
                self._convert_meal(m) for m in meals
            ])

    def _handle_update(self, method, url):
        """
        Handler for performing/checking for updates.

        Arguments:
        method -- HTTP method used when accessing the endpoint
        url -- urllib ParseResult containing the requested endpoint

        Returns:
        Tuple (HTTPStatus, JSON response string)

        Notes:
        This method supports the following URLs:
         - updates (GET: check if updates are necessary)
         - updates (POST: perform the update)
        """

        if method == 'GET':
            # Ask the processor for the update status
            try:
                needs_update = self._processor.check_update()
            except Exception as e:
                self._logger.exception("Update failed because of exception")
                return self._make_failure_object(
                    HTTPStatus.INTERNAL_SERVER_ERROR,
                    f"Update failed: {e}"
                )

            # Return it directly
            return self._make_success_object(needs_update)

        elif method == 'POST':
            # Return success without updating anything if no update is
            # necessary. Indicate the latter by returning 'false' as data.
            if not self._processor.check_update():
                return self._make_success_object(False)

            try:
                self._processor.perform_update()
            except Exception as e:
                self._logger.exception("Update failed because of exception")
                return self._make_failure_object(
                    HTTPStatus.INTERNAL_SERVER_ERROR,
                    f"Update failed: {e}"
                )

            # Successfully updated
            return self._make_success_object(True)

        else:
            # Unsupported method
            return self._make_failure_object(
                HTTPStatus.BAD_REQUEST,
                f"{method} method is not supported"
            )

    def _handle(self, method, url):
        """
        Handler for JSON API requests.

        Arguments:
        method -- HTTP method used when accessing the endpoint
        url -- urllib ParseResult containing the requested endpoint

        Returns:
        Tuple (HTTPStatus, JSON response string)

        Notes:
        There are two types of JSON responses:
         - Successful query: { 'success': True, 'data': {...} }
         - Failed query: { 'success': False, 'reason': 'why did it fail?' }
        Make sure to return an appropriate HTTP status code.
        For POST requests, no parameters are available.
        """

        # Must not be empty
        if url.path == '':
            return self._make_failure_object(
                HTTPStatus.BAD_REQUEST,
                f"No API endpoint specified"
            )

        # Handle restaurants
        elif url.path == 'restaurants' or url.path.startswith('restaurants/'):
            return self._handle_restaurants(method, url)

        # Handle meals
        elif url.path == 'meals' or url.path.startswith('meals/'):
            return self._handle_meals(method, url)

        # Handle database updates
        elif url.path == 'update' or url.path.startswith('update/'):
            return self._handle_update(method, url)

        # Invalid endpoint
        else:
            return self._make_failure_object(
                HTTPStatus.NOT_FOUND,
                f"Endpoint {url.path} does not exist"
            )

    def do_GET(self, url):
        """
        Handler for HTTP GET requests.

        Arguments:
        url -- urllib ParseResult containing the requested endpoint

        Returns:
        Tuple (HTTPStatus, JSON response string)
        """

        return self._handle('GET', url)

    def do_POST(self, url):
        """
        Handler for HTTP POST requests.

        Arguments:
        url -- urllib ParseResult containing the requested endpoint

        Returns:
        Tuple (HTTPStatus, JSON response string)
        """

        return self._handle('POST', url)
