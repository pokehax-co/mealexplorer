# -*- coding: utf-8 -*-
#
# Copyright (c) 2018 Thomas Weber <pokehaxco@gmail.com>
#
# SPDX-License-Identifier: MIT
#

#
# This module contains a class which wraps a SQLite database storing all
# information on restaurants and meals.
#

import re
import sqlite3

from abc import ABC, abstractmethod


"""
Rename sqlite3.Error to DatabaseError to avoid importing sqlite3 in other
modules, so that the underlying database technology can be changed later.
"""
DatabaseError = sqlite3.Error


class AbstractDatabase(ABC):
    """
    This class implements a database access object used by various types of
    databases (forum, blog, ...). The database instance (_database) and
    cursors derived from it should be hidden from the caller.
    """

    @abstractmethod
    def _create_tables(self, cursor):
        """
        Creates the tables for the database. They will be committed
        automatically after this function returns.

        Arguments:
        cursor -- Database cursor for this operation

        Returns:
        Nothing
        """

        return

    def __init__(self, database_filename):
        """
        Initializes a new database object.

        Arguments:
        database_filename -- Filename of the database to open/create

        Returns:
        Nothing

        Raises:
        DatabaseError -- if a database error occurs
        """

        self._database = sqlite3.connect(database_filename)

        # Return rows in query as sqlite3.Row, which allows both indexed and
        # named access to values.
        # https://docs.python.org/3/library/sqlite3.html
        self._database.row_factory = sqlite3.Row

    def __del__(self):
        """
        Commits unwritten changes into the database and closes it when the
        database object is purged from garbage collection.

        Arguments:
        None

        Returns:
        Nothing

        Raises:
        DatabaseError -- if a database error occurs
        """

        self._database.commit()
        self._database.close()

    def commit(self):
        """
        Commits unwritten changes into the database.

        Arguments:
        None

        Returns:
        Nothing

        Raises:
        DatabaseError -- if a database error occurs
        """

        self._database.commit()

    def create_tables(self):
        """
        Creates the tables required for the scraper.

        Arguments:
        None

        Returns:
        DatabaseStatus error code

        Raises:
        DatabaseError -- if a database error occurs
        """

        self._create_tables(self._database.cursor())
        self._database.commit()


class MealDatabase(AbstractDatabase):
    """
    This database class saves information on restaurants and the meals they
    offer.
    """

    def _create_tables(self, cursor):
        """
        Creates the tables for the database. They will be committed
        automatically after this function returns.

        Arguments:
        cursor -- Database cursor for this operation

        Returns:
        Nothing
        """

        #
        # Some of the fields are meant to be arrays. Due to SQLite limitations
        # (it does not support arrays), they will be represented as strings
        # internally and divided by the separator '|' (without '').
        #

        #
        # Restaurants table. Each restaurant has:
        #  - Numeric identifier (for API and other lookups)
        #  - Internal identifier (for the scrapers, to query the last update of
        #    the restaurant)
        #  - Name
        #  - Address (array with each line as a string)
        #  - Country (ISO 3166-1 alpha 2, like 'NL' for the Netherlands)
        #  - Phone number (international format, like +31 20 123456)
        #  - Link to website (we're scraping from them anyway)
        #  - Unix timestamp of last update (UTC). SQLite does not have a
        #    dedicated type for timestamps, so we use integers.
        #
        cursor.execute(
            ("create table if not exists mx_restaurants ("
             "id integer primary key autoincrement,"
             "scraper_id text,"
             "name text,"
             "address text,"
             "country character(2),"
             "phone text,"
             "website text,"
             "last_update integer"
             ")")
        )

        #
        # Meal in a restaurant. It is associated with a restaurant from the
        # restaurant table. It has:
        #  - Numeric identifier (for API and other lookups)
        #  - Name
        #  - Price (decimal, like 4.95)
        #  - Currency (ISO 4217, like EUR for Euro)
        #  - Ingredients (array)
        #
        cursor.execute(
            ("create table if not exists mx_meals ("
             "id integer primary key autoincrement,"
             "restaurant_id integer,"
             "name text,"
             "price real,"
             "currency character(3),"
             "ingredients text,"
             "foreign key(restaurant_id) references mx_restaurant(id)"
             ")")
        )

    #
    # Query operations
    #

    def query_restaurants_by_id(self, restaurant_id=None):
        """
        Generator function which queries restaurants by the ID in the database.
        If no ID is specified, all restaurants are returned.

        Arguments:
        restaurant_id -- ID of the restaurant to query (optional)

        Yields:
        SQLite rows of restaurants
        """

        cursor = self._database.cursor()

        if restaurant_id is not None:
            cursor.execute("select * from mx_restaurants where id=?",
                           (restaurant_id,))
        else:
            cursor.execute("select * from mx_restaurants")

        while True:
            results = cursor.fetchmany(64)
            if not results:
                break
            for result in results:
                yield result

    def query_restaurants_by_scraper_id(self, scraper_id):
        """
        Generator function which queries restaurants by the name of the scraper
        which added them to the database.

        Arguments:
        scraper_id -- Name of the scraper class

        Yields:
        SQLite rows of restaurants
        """

        cursor = self._database.cursor()
        cursor.execute(
            "select * from mx_restaurants where scraper_id=?",
            (scraper_id,)
        )

        # A scraper can support multiple restaurants
        while True:
            results = cursor.fetchmany(64)
            if not results:
                break
            for result in results:
                yield result

    def query_meals_by_id(self, meal_id=None):
        """
        Generator function which queries meals by the ID in the database.
        If no ID is specified, all meals are returned.

        Arguments:
        meal_id -- ID of the meal to query (optional)

        Yields:
        SQLite rows of meals
        """

        cursor = self._database.cursor()

        if meal_id is not None:
            cursor.execute("select * from mx_meals where id=?",
                           (meal_id,))
        else:
            cursor.execute("select * from mx_meals")

        while True:
            results = cursor.fetchmany(64)
            if not results:
                break
            for result in results:
                yield result

    def query_meals_by_search_terms(self, terms=[], search_field='name'):
        """
        Generator function which queries meals by the ID in the database.
        If no ID is specified, all meals are returned.

        Arguments:
        terms -- Search terms to look for in the database

        Yields:
        SQLite rows of meals
        """

        cursor = self._database.cursor()

        #
        # Build the query string. All terms must exist for an entry to appear
        # (by ANDing the conditionals). They're searched for case-insensitively
        # and may be contained as a substring ("like '%term%'").
        #
        query_cmd = 'select * from mx_meals'
        query_placeholders = []

        for i, term in enumerate(terms):
            query_cmd += ' where' if i == 0 else ' and'
            query_cmd += " lower(%s) like ?" % search_field
            query_placeholders.append("%%%s%%" % term.lower())

        cursor.execute(query_cmd, query_placeholders)

        while True:
            results = cursor.fetchmany(64)
            if not results:
                break
            for result in results:
                yield result

    #
    # Insertion/Update operations
    #

    def update_restaurant_last_update(self, restaurant_id, last_update):
        """
        Updates the 'last_update' column of the specified restaurant.

        Arguments:
        restaurant_id -- ID of the restaurant to update
        last_update -- Timestamp of the last update

        Returns:
        True if the update was successful (the row has been changed)
        False otherwise
        """

        # Strict type checking for update
        assert type(restaurant_id) is int
        assert type(last_update) is int

        cursor = self._database.cursor()
        cursor.execute(
            ("update mx_restaurants set last_update=? where id=?"),
            (last_update, restaurant_id)
        )

        return cursor.rowcount > 0

    def insert_restaurant(self, scraper_id, name, address, country, phone,
                          website, last_update):
        """
        Inserts a new restaurant into the database.

        Arguments:
        scraper_id -- Name of the scraper which added the restaurant.
        name -- Name of the restaurant.
        address -- Array of strings with address components
        country -- ISO-3166 code of the restaurant's country
        phone -- Phone number in international format
        website -- Link to the restaurant's website
        last_update -- Timestamp of the last update

        Returns:
        ID of newly inserted restaurant
        """

        # Strict type checking for insertion
        assert type(scraper_id) is str and len(scraper_id) > 0
        assert type(name) is str and len(name) > 0
        assert type(address) in (list, tuple)
        assert len(country) == 2 and country[0] >= 'A' and country[0] <= 'Z'
        assert country[1] >= 'A' and country[1] <= 'Z'
        assert type(phone) is str and re.match(r'^\+[1-9][0-9]+$', phone)
        assert type(website) is str and re.match(r'^https?://', website)
        assert type(last_update) is int

        # Array -> String
        address = "|".join(address)

        cursor = self._database.cursor()
        cursor.execute(
            ("insert into mx_restaurants (scraper_id,name,address,country,"
             "phone,website,last_update) values (?,?,?,?,?,?,?)"),
            (scraper_id, name, address, country, phone, website, last_update)
        )

        # Last modified row in this cursor == new ID
        return cursor.lastrowid

    def insert_meal(self, restaurant_id, name, price, currency, ingredients):
        """
        Inserts a new meal into the database. If it already exists in the
        database, no insertion will be performed.

        Arguments:
        restaurant_id -- ID of the restaurant offering the meal
        name -- Name of the meal
        price -- Price of the meal
        currency -- ISO-4217 code of the currency
        ingredients -- List of ingredients contained in the meal

        Returns:
        ID of newly inserted meal, or ID of same meal which already exists
        """

        # Strict type checking for insertion
        assert type(restaurant_id) is int and restaurant_id > 0
        assert type(name) is str and len(name) > 0
        assert type(price) is float and price >= 0.
        assert type(currency) is str and re.match(r'^[A-Z]{3}$', currency)
        assert ingredients is None or type(ingredients) in (list, tuple, set)

        # Array -> String. Sort the ingredients to make the meals more
        # deterministic.
        if ingredients:
            ingredients = "|".join(sorted(ingredients))

        cursor = self._database.cursor()

        # Check for pre-existing entry. We can't use col=? to check for null
        # entries (if there are no "ingredients", its value is null), so we
        # have to use the 'col is null' check in this case.
        if ingredients:
            cursor.execute(
                ("select * from mx_meals where restaurant_id=? and name=? and "
                 "price=? and currency=? and ingredients=?"),
                (restaurant_id, name, price, currency, ingredients)
            )
        else:
            cursor.execute(
                ("select * from mx_meals where restaurant_id=? and name=? and "
                 "price=? and currency=? and ingredients is null"),
                (restaurant_id, name, price, currency)
            )

        # Return the existing ID, don't write another entry
        result = cursor.fetchone()
        if result:
            return result['id']

        # Otherwise, insert the new meal
        cursor.execute(
            ("insert into mx_meals (restaurant_id,name,price,currency,"
             "ingredients) values (?,?,?,?,?)"),
            (restaurant_id, name, price, currency, ingredients)
        )

        # Last modified row in this cursor == new ID
        return cursor.lastrowid
