# -*- coding: utf-8 -*-
#
# Copyright (c) 2018 Thomas Weber <pokehaxco@gmail.com>
#
# SPDX-License-Identifier: MIT
#

#
# Main program of Meal Explorer, which implements argument parsing and operates
# the individual components.
#

import argparse
import logging
import os

from .backend import Processor
from .database import DatabaseError, MealDatabase
from .frontend import FrontendServer
from .rest_api import RestAPI


class MealExplorer(object):
    """
    This class implements the main program of Meal Explorer. It is called from
    __main__.py when run with 'python3 -m mealexplorer'.
    """

    def __init__(self):
        """
        Class constructor. Because the object is only created once, it can be
        considered as static.

        Arguments:
        None

        Returns:
        Nothing
        """

        # Configure the logger
        logging.basicConfig(
            level=logging.DEBUG,
            format='[%(name)s %(levelname)s] %(message)s'
        )

        # Initialize one for this class
        self.logger = logging.getLogger(type(self).__name__)

    def run(self, argv):
        """
        Program entry point. Runs the entire application.

        Arguments:
        argv -- List of program arguments the application is started with

        Returns:
        0 if the application terminates successfully
        Non-zero if the application terminates because of an error
        """

        #
        # Parse program arguments
        #

        argp = argparse.ArgumentParser(
            description=('Operates a web application offering and scraping '
                         'meals from restaurant websites.')
        )

        argp.add_argument('--database-path', type=str,
                          default='/var/lib/mealexplorer/database.sqlite',
                          help='path to the backing SQLite database')
        argp.add_argument('--listen-address', type=str,
                          default='127.0.0.1',
                          help='listening address for the server')
        argp.add_argument('--listen-port', type=int, choices=range(1, 65536),
                          default=11111, metavar="LISTEN_PORT",
                          help='listening port (1-65535) for the server')
        argp.add_argument('--no-internet', action='store_true',
                          help='disables internet access for the scrapers')
        argp.set_defaults(no_internet=False)

        # parse_args() uses argv[1:] if none are specified, see Python source
        self.args = argp.parse_args(argv[1:])

        #
        # Create the database
        #

        self.logger.debug(("creating/opening database "
                           f"{self.args.database_path}"))

        self.database = MealDatabase(self.args.database_path)
        try:
            self.database.create_tables()
        except DatabaseError as e:
            self.logger.error(f"cannot create database: {e}")
            return 1

        #
        # Initialize the backend and the REST API
        #

        self.processor = Processor(self.database, self.args.no_internet)
        self.rest_api = RestAPI(self.processor, self.database)

        #
        # Run the HTTP server
        #

        htdocs_path = os.path.join(os.path.dirname(__file__), 'htdocs')
        self.server = FrontendServer(self.args.listen_address,
                                     self.args.listen_port, htdocs_path,
                                     self.rest_api)
        self.server.run()

        return 0
