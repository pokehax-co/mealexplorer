# -*- coding: utf-8 -*-
#
# Copyright (c) 2018 Thomas Weber <pokehaxco@gmail.com>
#
# SPDX-License-Identifier: MIT
#

#
# This module implements the backend of Meal Explorer.
#

import inspect
import logging
import time

from . import scraping


class Processor(object):
    """
    This class implements the processing module, which goes through the
    database and the scrapers and performs the update.
    """

    # 24 hours
    UPDATE_INTERVAL = 86400

    @staticmethod
    def _is_scraper_class(o):
        """
        Static method which filters only the following objects from the
        scraping module:
         - Must be a class
         - Must be derived from AbstractScraper
         - Must not be AbstractScraper itself

        Arguments:
        o -- Object to verify

        Returns:
        True if the object fulfills the above conditions
        False otherwise
        """

        return (
            inspect.isclass(o) and issubclass(o, scraping.AbstractScraper) and
            o.__name__ != 'AbstractScraper'
        )

    def __init__(self, database, no_internet):
        """
        Initializes a new backend processor.

        Arguments:
        database -- Instance of the MealDatabase to operate with
        no_internet -- True to disable internet access for the scrapers,
                       otherwise False

        Returns:
        Nothing
        """

        self._database = database
        self._no_internet = no_internet

        # Initialize scraper for this class
        self._logger = logging.getLogger(type(self).__name__)

        if self._no_internet:
            self._logger.warn("scrapers cannot access the internet!")

        # Collect the scraper classes implemented in scraper.py
        self._scrapers = inspect.getmembers(scraping,
                                            Processor._is_scraper_class)

    def check_update(self):
        """
        Goes through the database and looks for the two conditions under which
        an update occurs:
         - A scraper didn't add a restaurant (and therefore no meals)
         - The last update is more than UPDATE_INTERVAL ago

        Arguments:
        silent -- When set to True, suppresses logging output (optional)

        Returns:
        True if an update is required
        False otherwise

        Raises:
        DatabaseError -- if a database error occurs
        """

        update_required = False

        for scraper_id, scraper_class in self._scrapers:
            self._logger.info(f"checking for updates to {scraper_id}")

            # Find the scraper's restaurants. If we fail, we must update
            have_restaurants = False
            for r in self._database.query_restaurants_by_scraper_id(
                    scraper_id):
                have_restaurants = True

                # Check for expiration and update in this case, too
                if time.time() - Processor.UPDATE_INTERVAL >= r['last_update']:
                    self._logger.debug(
                        f"restaurant '{r['name']}' must be updated"
                    )
                    update_required = True

            if not have_restaurants:
                self._logger.debug(f"{scraper_id} has no restaurants")
                update_required = True

        self._logger.info(f"update of restaurants required: {update_required}")
        return update_required

    def perform_update(self):
        """
        Updates the restaurants which need an update, and run the scrapers
        which have not inserted a restaurant yet.

        Arguments:
        None

        Returns:
        Nothing if the update was successful
        Raises an exception otherwise

        Raises:
        Exception -- if an error occurs
        """

        for scraper_id, scraper_class in self._scrapers:
            # Find the scraper's restaurants. If we fail, we must update
            have_restaurants = False
            for r in self._database.query_restaurants_by_scraper_id(
                    scraper_id):
                have_restaurants = True

                # Check for expiration and update in this case, too
                if time.time() - Processor.UPDATE_INTERVAL >= r['last_update']:
                    self._logger.info(
                        f"updating restaurant '{r['name']}' of {scraper_id}"
                    )
                    scraper = scraper_class(self._database, self._no_internet)
                    scraper.update(False)

            if not have_restaurants:
                self._logger.debug(f"performing first scrape for {scraper_id}")
                scraper = scraper_class(self._database, self._no_internet)
                scraper.update(True)

        self._logger.info(f"update successful")
