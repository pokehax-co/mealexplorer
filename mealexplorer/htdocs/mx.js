/*
 * Copyright (c) 2018 Thomas Weber <pokehaxco@gmail.com>
 *
 * SPDX-License-Identifier: MIT
 */

'use strict';

(function() {
    /* Cache to store restaurant names */
    var g_restaurant_names = {};

    /*
     * ISO-3166 country identifier -> country name.
     * From https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2, with country
     * names slightly simplified.
     */
    var g_country_by_iso3166 = {
        'AD': 'Andorra',
        'AE': 'United Arab Emirates',
        'AF': 'Afghanistan',
        'AG': 'Antigua and Barbuda',
        'AI': 'Anguilla',
        'AL': 'Albania',
        'AM': 'Armenia',
        'AO': 'Angola',
        'AQ': 'Antarctica',
        'AR': 'Argentina',
        'AS': 'American Samoa',
        'AT': 'Austria',
        'AU': 'Australia',
        'AW': 'Aruba',
        'AX': 'Åland Islands',
        'AZ': 'Azerbaijan',
        'BA': 'Bosnia and Herzegovina',
        'BB': 'Barbados',
        'BD': 'Bangladesh',
        'BE': 'Belgium',
        'BF': 'Burkina Faso',
        'BG': 'Bulgaria',
        'BH': 'Bahrain',
        'BI': 'Burundi',
        'BJ': 'Benin',
        'BL': 'Saint Barthélemy',
        'BM': 'Bermuda',
        'BN': 'Brunei Darussalam',
        'BO': 'Bolivia',
        'BQ': 'Bonaire, Sint Eustatius and Saba',
        'BR': 'Brazil',
        'BS': 'Bahamas',
        'BT': 'Bhutan',
        'BV': 'Bouvet Island',
        'BW': 'Botswana',
        'BY': 'Belarus',
        'BZ': 'Belize',
        'CA': 'Canada',
        'CC': 'Cocos (Keeling) Islands',
        'CD': 'Democratic Republic of the Congo',
        'CF': 'Central African Republic',
        'CG': 'Congo',
        'CH': 'Switzerland',
        'CI': 'Côte d\'Ivoire',
        'CK': 'Cook Islands',
        'CL': 'Chile',
        'CM': 'Cameroon',
        'CN': 'China',
        'CO': 'Colombia',
        'CR': 'Costa Rica',
        'CU': 'Cuba',
        'CV': 'Cabo Verde',
        'CW': 'Curaçao',
        'CX': 'Christmas Island',
        'CY': 'Cyprus',
        'CZ': 'Czechia',
        'DE': 'Germany',
        'DJ': 'Djibouti',
        'DK': 'Denmark',
        'DM': 'Dominica',
        'DO': 'Dominican Republic',
        'DZ': 'Algeria',
        'EC': 'Ecuador',
        'EE': 'Estonia',
        'EG': 'Egypt',
        'EH': 'Western Sahara',
        'ER': 'Eritrea',
        'ES': 'Spain',
        'ET': 'Ethiopia',
        'FI': 'Finland',
        'FJ': 'Fiji',
        'FK': 'Falkland Islands',
        'FM': 'Micronesia',
        'FO': 'Faroe Islands',
        'FR': 'France',
        'GA': 'Gabon',
        'GB': 'United Kingdom',
        'GD': 'Grenada',
        'GE': 'Georgia',
        'GF': 'French Guiana',
        'GG': 'Guernsey',
        'GH': 'Ghana',
        'GI': 'Gibraltar',
        'GL': 'Greenland',
        'GM': 'Gambia',
        'GN': 'Guinea',
        'GP': 'Guadeloupe',
        'GQ': 'Equatorial Guinea',
        'GR': 'Greece',
        'GS': 'South Georgia and the South Sandwich Islands',
        'GT': 'Guatemala',
        'GU': 'Guam',
        'GW': 'Guinea-Bissau',
        'GY': 'Guyana',
        'HK': 'Hong Kong',
        'HM': 'Heard Island and McDonald Islands',
        'HN': 'Honduras',
        'HR': 'Croatia',
        'HT': 'Haiti',
        'HU': 'Hungary',
        'ID': 'Indonesia',
        'IE': 'Ireland',
        'IL': 'Israel',
        'IM': 'Isle of Man',
        'IN': 'India',
        'IO': 'British Indian Ocean Territory',
        'IQ': 'Iraq',
        'IR': 'Iran',
        'IS': 'Iceland',
        'IT': 'Italy',
        'JE': 'Jersey',
        'JM': 'Jamaica',
        'JO': 'Jordan',
        'JP': 'Japan',
        'KE': 'Kenya',
        'KG': 'Kyrgyzstan',
        'KH': 'Cambodia',
        'KI': 'Kiribati',
        'KM': 'Comoros',
        'KN': 'Saint Kitts and Nevis',
        'KP': 'North Korea',
        'KR': 'South Korea',
        'KW': 'Kuwait',
        'KY': 'Cayman Islands',
        'KZ': 'Kazakhstan',
        'LA': 'Laos',
        'LB': 'Lebanon',
        'LC': 'Saint Lucia',
        'LI': 'Liechtenstein',
        'LK': 'Sri Lanka',
        'LR': 'Liberia',
        'LS': 'Lesotho',
        'LT': 'Lithuania',
        'LU': 'Luxembourg',
        'LV': 'Latvia',
        'LY': 'Libya',
        'MA': 'Morocco',
        'MC': 'Monaco',
        'MD': 'Moldova',
        'ME': 'Montenegro',
        'MF': 'Saint Martin (French part)',
        'MG': 'Madagascar',
        'MH': 'Marshall Islands',
        'MK': 'Macedonia',
        'ML': 'Mali',
        'MM': 'Myanmar',
        'MN': 'Mongolia',
        'MO': 'Macao',
        'MP': 'Northern Mariana Islands',
        'MQ': 'Martinique',
        'MR': 'Mauritania',
        'MS': 'Montserrat',
        'MT': 'Malta',
        'MU': 'Mauritius',
        'MV': 'Maldives',
        'MW': 'Malawi',
        'MX': 'Mexico',
        'MY': 'Malaysia',
        'MZ': 'Mozambique',
        'NA': 'Namibia',
        'NC': 'New Caledonia',
        'NE': 'Niger',
        'NF': 'Norfolk Island',
        'NG': 'Nigeria',
        'NI': 'Nicaragua',
        'NL': 'Netherlands',
        'NO': 'Norway',
        'NP': 'Nepal',
        'NR': 'Nauru',
        'NU': 'Niue',
        'NZ': 'New Zealand',
        'OM': 'Oman',
        'PA': 'Panama',
        'PE': 'Peru',
        'PF': 'French Polynesia',
        'PG': 'Papua New Guinea',
        'PH': 'Philippines',
        'PK': 'Pakistan',
        'PL': 'Poland',
        'PM': 'Saint Pierre and Miquelon',
        'PN': 'Pitcairn',
        'PR': 'Puerto Rico',
        'PS': 'Palestine',
        'PT': 'Portugal',
        'PW': 'Palau',
        'PY': 'Paraguay',
        'QA': 'Qatar',
        'RE': 'Réunion',
        'RO': 'Romania',
        'RS': 'Serbia',
        'RU': 'Russian Federation',
        'RW': 'Rwanda',
        'SA': 'Saudi Arabia',
        'SB': 'Solomon Islands',
        'SC': 'Seychelles',
        'SD': 'Sudan',
        'SE': 'Sweden',
        'SG': 'Singapore',
        'SH': 'Saint Helena, Ascension and Tristan da Cunha',
        'SI': 'Slovenia',
        'SJ': 'Svalbard and Jan Mayen',
        'SK': 'Slovakia',
        'SL': 'Sierra Leone',
        'SM': 'San Marino',
        'SN': 'Senegal',
        'SO': 'Somalia',
        'SR': 'Suriname',
        'SS': 'South Sudan',
        'ST': 'Sao Tome and Principe',
        'SV': 'El Salvador',
        'SX': 'Sint Maarten (Dutch part)',
        'SY': 'Syrian Arab Republic',
        'SZ': 'Swaziland',
        'TC': 'Turks and Caicos Islands',
        'TD': 'Chad',
        'TF': 'French Southern Territories',
        'TG': 'Togo',
        'TH': 'Thailand',
        'TJ': 'Tajikistan',
        'TK': 'Tokelau',
        'TL': 'Timor-Leste',
        'TM': 'Turkmenistan',
        'TN': 'Tunisia',
        'TO': 'Tonga',
        'TR': 'Turkey',
        'TT': 'Trinidad and Tobago',
        'TV': 'Tuvalu',
        'TW': 'Taiwan',
        'TZ': 'Tanzania',
        'UA': 'Ukraine',
        'UG': 'Uganda',
        'UM': 'United States Minor Outlying Islands',
        'US': 'United States of America',
        'UY': 'Uruguay',
        'UZ': 'Uzbekistan',
        'VA': 'Holy See',
        'VC': 'Saint Vincent and the Grenadines',
        'VE': 'Venezuela',
        'VG': 'British Virgin Islands',
        'VI': 'U.S. Virgin Islands',
        'VN': 'Vietnam',
        'VU': 'Vanuatu',
        'WF': 'Wallis and Futuna',
        'WS': 'Samoa',
        'YE': 'Yemen',
        'YT': 'Mayotte',
        'ZA': 'South Africa',
        'ZM': 'Zambia',
        'ZW': 'Zimbabwe'
    };

    /*
     * Shorthand for document.getElementById().
     *
     * Arguments:
     * e -- ID of the DOM element to fetch
     *
     * Returns:
     * DOM element whose ID is e
     */
    function get_element(e) {
        return document.getElementById(e);
    }

    /*
     * Removes all child nodes of the current element.
     *
     * Arguments:
     * element -- Element whose child nodes are to be removed
     *
     * Returns:
     * Nothing
     */
    function remove_all_children(element) {
        while (element.firstChild)
            element.removeChild(element.firstChild);
    }

    /*
     * Performs an AJAX request to an URL delivering JSON data.
     *
     * Arguments:
     * method -- Method to use
     * url -- URL to load
     * callback -- Callback to execute when the page loaded successfully
     * error -- Callback to execute when the page load failed
     *
     * Returns:
     * Created AJAX object
     *
     * Notes:
     * The AJAX object is passed as parameter to both callback functions.
     * The returned JSON object is the normal callback's 2nd argument.
     * The reason for the error is the error callback's 2nd argument.
     */
    function call_rest_api(method, url, callback, error) {
        var ajax = new XMLHttpRequest();
        ajax.open(method, url);
        ajax.responseType = 'json';
        ajax.json = true;   /* IE11 does not support responseType 'json' */
        ajax.onreadystatechange = function() {
            if (ajax.readyState == 4) {
                if (ajax.status == 200) {
                    var json_data;
                    if (typeof ajax.response === 'string')
                        json_data = JSON.parse(ajax.response);
                    else
                        json_data = ajax.response;

                    if (json_data.success)
                        callback(ajax, json_data);
                    else
                        error(ajax, json_data.reason);
                } else {
                    error(ajax, 'Server returned HTTP code ' + ajax.status);
                }
            }
        }
        ajax.send();
        return ajax;
    }

    /*
     * Checks whether two meals are equal.
     *
     * Arguments:
     * a -- First meal (from the REST API 'meals' query)
     * b -- Second meal (from the REST API 'meals' query)
     *
     * Returns:
     * true if the meals are identical
     * false otherwise
     */
    function meals_are_equal(a, b) {
        /* Case-insensitive name checking */
        return a.name.toLowerCase() == b.name.toLowerCase();
    }

    /*
     * Transforms the list of meals so that same meals of different restaurants
     * are combined in a single entry.
     *
     * Arguments:
     * meals -- Array of meals (from the REST API 'meals' query)
     *
     * Returns:
     * New array returning meals with same meals from different restaurants
     * merged into one
     */
    function search_transform_meals(meals) {
        var new_meals = [];

        var old_meals_length = meals.length;
        while (meals.length > 0) {
            /* Remove current meal from list */
            var old_meal = meals.shift();

            /* Move restaurant ID, price, currency, ingredients to an array */
            var new_meal = {
                'name': old_meal.name,
                'offers': [{
                    'restaurant_id': old_meal.restaurant_id,
                    'price': old_meal.price,
                    'currency': old_meal.currency,
                    'ingredients': old_meal.ingredients
                }]
            };

            /* Merge with existing meals by comparing against them. O(n^2) */
            for (var i = 0; i < meals.length;) {
                if (meals_are_equal(old_meal, meals[i])) {
                    /* Pop meal off. Next element is at same i */
                    var merging_meal = meals.splice(i, 1)[0];

                    /* Add offer attributes to current meal */
                    new_meal.offers.push({
                        'restaurant_id': merging_meal.restaurant_id,
                        'price': merging_meal.price,
                        'currency': merging_meal.currency,
                        'ingredients': merging_meal.ingredients
                    });
                } else {
                    ++i;
                }
            }

            new_meals.push(new_meal);
        }
        return new_meals;
    }

    /*
     * Updates the <p> elements presenting the status of the search.
     *
     * Arguments:
     * state -- 0 if no search string has been entered,
     *          1 if no results have been found for this search,
     *          2 if the search failed because of a server error,
     *          3 to show the search results.
     *
     * Returns:
     * Nothing
     */
    function search_show_element_post(state) {
        var elements = ['search-show-all', 'search-no-results',
            'search-failed', 'search-results'];

        for (var i = 0; i < elements.length; ++i) {
            get_element(elements[i]).style.display = (i == state) ?
                'block' : 'none';
        }
    }

    /*
     * Loads the restaurant information via AJAX request and shows them in a
     * popup.
     *
     * Arguments:
     * restaurant_id -- ID of the restaurant to fetch
     *
     * Returns:
     * Nothing
     */
    function restaurant_show_popup(restaurant_id) {
        call_rest_api(
            'GET',
            'api/restaurants/' + restaurant_id,
            function(ajax, response) {
                var restaurant = response.data;

                /* Fill information in. The tel: link for the phone number is
                   the reason why the format has been restricted. */
                get_element('restaurant-name').textContent = restaurant.name;
                get_element('restaurant-phone').textContent = restaurant.phone;
                get_element('restaurant-phone').setAttribute('href',
                    'tel:' + restaurant.phone);
                get_element('restaurant-link').textContent = restaurant.website;
                get_element('restaurant-link').setAttribute('href',
                    restaurant.website);

                /* Add address parts */
                var address_span = get_element('restaurant-address');
                remove_all_children(address_span);

                for (var i = 0; i < restaurant.address.length; ++i) {
                    address_span.appendChild(document.createTextNode(
                        restaurant.address[i]));
                    address_span.appendChild(document.createElement('br'));
                }

                /* Add country to address */
                var country = (restaurant.country in g_country_by_iso3166) ?
                    g_country_by_iso3166[restaurant.country] :
                    '[Country ' + restaurant.country + ']';
                address_span.appendChild(document.createTextNode(country));

                /* Show the popup */
                get_element('restaurant-info').style.display = 'block';
            },
            function(ajax, reason) {
                window.alert('Cannot show restaurant information: ' + reason);
            }
        );
    }

    /*
     * Fills the list of a meal with the restaurants offering the meal.
     *
     * Arguments:
     * restaurant_list -- DOM <ul> element which has the meal attached in an
     *                    'associated_meal' attribute
     *
     * Returns:
     * Nothing
     */
    function search_add_result_offers(restaurant_list) {
        var meal = restaurant_list.associated_meal;

        for (var i = 0; i < meal.offers.length; ++i) {
            var offer = meal.offers[i];
            var restaurant_list_entry = document.createElement('li');

            /* Create link to restaurant. It will popup the contact
               information */
            var restaurant_link = document.createElement('a');
            restaurant_link.restaurant_id = offer.restaurant_id;
            restaurant_link.setAttribute('href', '');

            if (offer.restaurant_id in g_restaurant_names) {
                /* Load name from cache */
                restaurant_link.textContent = g_restaurant_names[
                    offer.restaurant_id
                ];
            } else {
                /* Show ID if name not in cache */
                restaurant_link.textContent =
                    '[Restaurant ID: ' + offer.restaurant_id + ']';
            }

            /* Show restaurant popup when clicking on it */
            restaurant_link.addEventListener('click', function(e) {
                /* Use preventDefault() to prevent navigation from this link */
                e.preventDefault();
                restaurant_show_popup(e.target.restaurant_id);
            }, false);

            /* Add text node with pricing and ingredient info */
            var ingredients = 'no ingredients known';
            if (offer.ingredients.length > 0) {
                ingredients = 'ingredients: ' +
                    offer.ingredients.join(', ');
            }

            /* Show price as exactly two digits */
            var price = parseFloat(
                Math.round(offer.price * 100) / 100
            ).toFixed(2);

            var offer_info = document.createTextNode(
                ' (price: ' + offer.currency + ' ' + price + ', ' +
                ingredients + ')'
            );

            /* Add elements to list entry */
            restaurant_list_entry.appendChild(restaurant_link);
            restaurant_list_entry.appendChild(offer_info);
            restaurant_list.appendChild(restaurant_list_entry);
        }
    }

    /*
     * Adds a new meal to the search results.
     *
     * Arguments:
     * results_p -- <p> element of the search result paragraph
     * i -- Current index inside the meal list (search_transform_meals())
     * meal -- Meal to add (search_transform_meals())
     *
     * Returns:
     * Nothing
     */
    function search_add_result(results_p, i, meal) {
        /* Wrap meal in a <div> */
        var div = document.createElement('div');

        /* Create hidden list in which the restaurants should be added */
        var restaurant_list = document.createElement('ul');
        restaurant_list.style.display = 'none';

        /* Don't create it now, only when expanded the first time. Save the
           offers in the list object. */
        restaurant_list.associated_meal = meal;

        /* Create a bold heading */
        var heading_strong = document.createElement('strong');
        heading_strong.textContent = meal.name;

        /* Create a link (number of restaurants) which expands the restaurants
           offering this meal */
        var heading_link = document.createElement('a');
        heading_link.setAttribute('href', '');

        heading_link.textContent = '(offered by ' + meal.offers.length +
            ' restaurant' + (meal.offers.length == 1 ? '' : 's') + ')';

        heading_link.addEventListener('click', function(e) {
            /* Use preventDefault() to prevent navigation from this link */
            e.preventDefault();

            /* Hide the list (next sibling of link) */
            if (e.target.nextSibling.style.display == 'block')
                e.target.nextSibling.style.display = 'none';

            /* Show the list */
            else {
                e.target.nextSibling.style.display = 'block';

                /* Populate the list the first time it is shown */
                if (!e.target.nextSibling.hasChildNodes())
                    search_add_result_offers(e.target.nextSibling);
            }
        }, 'false');


        /* Add the elements to the <div> */
        div.appendChild(heading_strong);
        div.appendChild(document.createTextNode(' '));
        div.appendChild(heading_link);
        div.appendChild(restaurant_list);

        results_p.appendChild(div);
    }

    /*
     * Performs a search operation whenever the search box or the option
     * changes.
     *
     * Arguments:
     * force_empty -- Set to true to show all meals (by searching for ''),
     *                used for the 'Show all meals' link handler.
     *
     * Returns:
     * Nothing
     */
    function do_search(force_empty) {
        /* Read form values */
        var search_text = document.querySelector(
            'input[name="q"]').value.trim();
        var search_in = document.querySelector(
            'input[name="in"]:checked').value;

        /* Get all the elements we need */
        var search_failed_r = get_element('search-failed-reason');
        var results_p = get_element('search-results');

        /* If there is no input, show the 'show all' link */
        if (!force_empty && search_text === '') {
            search_show_element_post(0);
            remove_all_children(results_p);
            return;
        }

        /* Build the URL for the REST API */
        var endpoint_url = 'api/meals' + ((search_text === '') ? '' : (
            '?q=' + encodeURIComponent(search_text) +
            '&in=' + encodeURIComponent(search_in)));

        call_rest_api(
            'GET',
            endpoint_url,
            function(ajax, response) {
                /* No results? */
                if (response.data.length == 0) {
                    search_show_element_post(1);
                    return;
                }

                /* Clear existing list and add the new meals */
                remove_all_children(results_p);
                var meals = search_transform_meals(response.data);
                for (var i = 0; i < meals.length; ++i)
                    search_add_result(results_p, i, meals[i]);

                search_show_element_post(3);
            },
            function(ajax, reason) {
                /* Show error reason */
                search_failed_r.textContent = reason;
                remove_all_children(results_p);
                search_show_element_post(2);
            }
        );
    }

    /*
     * Selects the <p> element to display when an update is to be performed.
     *
     * Arguments:
     * check_ok -- Set to true to display an element indicating the update
     *             check did not fail. If this value is false, an element
     *             indicating an error is displayed.
     * updates_needed -- Set to true to show an element where the update can
     *                   be performed, false to show an element indicating that
     *                   an update is not necessary.
     *
     * Returns:
     * Nothing
     */
    function update_show_element_pre(actually_updating) {
        get_element('update-check').style.display = actually_updating ?
            'none' : 'block';
        get_element('update-progress').style.display = actually_updating ?
            'block' : 'none';
        get_element('update-failed').style.display = 'none';
        get_element('update-unnecessary').style.display = 'none';
        get_element('update-necessary').style.display = 'none';
    }

    /*
     * Selects the <p> element to display when an update has been performed.
     *
     * Arguments:
     * check_ok -- Set to true to display an element indicating the update
     *             check did not fail. If this value is false, an element
     *             indicating an error is displayed.
     * updates_needed -- Set to true to show an element where the update can
     *                   be performed, false to show an element indicating that
     *                   an update is not necessary.
     *
     * Returns:
     * Nothing
     */
    function update_show_element_post(check_ok, updates_needed) {
        get_element('update-check').style.display = 'none';
        get_element('update-progress').style.display = 'none';
        get_element('update-failed').style.display = check_ok ?
            'none' : 'block';
        get_element('update-unnecessary').style.display =
            (check_ok && !updates_needed) ? 'block' : 'none';
        get_element('update-necessary').style.display =
            (check_ok && updates_needed) ? 'block' : 'none';
    }

    /*
     * Performs an update check and displays the correct <p> element in the
     * web page.
     *
     * Arguments:
     * None
     *
     * Returns:
     * Nothing
     */
    function check_for_updates() {
        update_show_element_pre(false);
        call_rest_api(
            'GET',
            'api/update',
            function(ajax, response) {
                update_show_element_post(true, response.data);
            },
            function(ajax, reason) {
                update_show_element_post(false, false);
            }
        );
    }

    /*
     * Performs an update and displays the correct <p> element in the web page.
     *
     * Arguments:
     * None
     *
     * Returns:
     * Nothing
     */
    function perform_update() {
        update_show_element_pre(true);
        call_rest_api(
            'POST',
            'api/update',
            function(ajax, response) {
                check_for_updates();

                /* Update restaurant names, then perform a search operation.
                   Clear the search field after an update. */
                get_element('search-text').value = '';
                update_restaurant_names(do_search);
            },
            function(ajax, reason) {
                update_show_element_post(false, false);
            }
        );
    }

    /*
     * Updates the list of all restaurant names in the database, to reduce the
     * number of queries.
     *
     * Arguments:
     * success_cb -- Callback to execute when the update is successful
     *
     * Returns:
     * Nothing
     */
    function update_restaurant_names(success_cb) {
        call_rest_api(
            'GET',
            'api/restaurants?only_names=1',
            function(ajax, response) {
                /* Transform JSON response data to dictionary */
                g_restaurant_names = {};
                for (var i = 0; i < response.data.length; ++i) {
                    var entry = response.data[i];
                    g_restaurant_names[entry.id] = entry.name;
                }

                /* Now we can perform operations based on g_restaurant_names */
                if (success_cb)
                    success_cb();
            },
            function(ajax, reason) {
                g_restaurant_names = {};
                window.alert('Cannot query restaurant names: ' + reason);
            }
        );
    }

    /* Set up callback when the page is loaded. We add some actions to the
       elements on the page. */
    document.addEventListener('DOMContentLoaded', function() {
        check_for_updates();

        /* Update button */
        get_element('update-button').addEventListener(
            'click', perform_update, false);

        /* Search fields */
        get_element('search-in-meals').addEventListener(
            'click', function() { do_search(false); }, false);
        get_element('search-in-ingredients').addEventListener(
            'click', function() { do_search(false); }, false);
        get_element('search-text').addEventListener(
            'input', function() { do_search(false); }, false);

        get_element('search-show-all-link').addEventListener(
            'click', function(e) {
                /* Use preventDefault() to prevent navigation from this link */
                e.preventDefault();
                do_search(true);
            }, false);

        /* Hide restaurant popup */
        get_element('restaurant-info-close').addEventListener(
            'click', function(e) {
                /* Use preventDefault() to prevent navigation from this link */
                e.preventDefault();
                get_element('restaurant-info').style.display = 'none';
            }, false);

        /* Update restaurant names, then perform a search operation */
        update_restaurant_names(do_search);
    }, false);
})();
