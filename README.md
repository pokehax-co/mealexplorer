# Meal Explorer

Web application written in Python which scrapes restaurant menus from the Internet and aggregates them in a single page.

## Browser compatibility

* Chrome 67
* Edge (Windows 10 1709)
* Firefox 60
* Internet Explorer 11
* Opera 12.18

## Installation and running instructions for Ubuntu 18.04

Tested with a minimal installation from the network (no GUI, no server packages).

* Install dependencies: `sudo apt install p7zip python3-bs4 python3-pycurl python3-html5lib`
* Unpack the archive: `p7zip -d mealexplorer.7z`
* Show supported command line arguments: `python3 -m mealexplorer --help`
* Start the application with a local database: `python3 -m mealexplorer --database-path=./database.sqlite`

To change the listening address and the port, use the `--listen-address` and `--listen-port` arguments.

If the `--database-path` argument is not specified, the database will be stored in `/var/lib/mealexplorer`, which must be writable by the user who runs Meal Explorer.
